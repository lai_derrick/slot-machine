var gifts = [
  { type: "image", name: "張博傑", path: "image/張博傑.jpg" },
  { type: "image", name: "俞曦林", path: "image/俞曦林.jpg" },
  { type: "image", name: "劉書暉", path: "image/劉書暉.jpg" },
  { type: "image", name: "劉惠卿", path: "image/劉惠卿.jpg" },
  { type: "image", name: "陳盈如", path: "image/陳盈如.jpg" },
  { type: "image", name: "蕭思羽", path: "image/蕭思羽.jpg" },
  { type: "image", name: "邱伃廷", path: "image/邱伃廷.jpg" },
  { type: "image", name: "李宜樺", path: "image/李宜樺.jpg" },
  { type: "image", name: "王雅亭", path: "image/王雅亭.jpg" },
  { type: "image", name: "蔡雯婷", path: "image/蔡雯婷.jpg" },
  { type: "image", name: "吳采玲", path: "image/吳采玲.jpg" },
  { type: "image", name: "連竟成", path: "image/連竟成.jpg" },
  { type: "image", name: "陳秉鈞", path: "image/陳秉鈞.jpg" },
  { type: "image", name: "歐怡廷", path: "image/歐怡廷.jpg" },
  { type: "image", name: "郭乃綺", path: "image/郭乃綺.jpg" },
  { type: "image", name: "張伶憶", path: "image/張伶憶.jpg" },
  { type: "image", name: "謝沂臻", path: "image/謝沂臻.jpg" },
  { type: "image", name: "賴德福", path: "image/賴德福.jpg" },
  { type: "image", name: "吳盈蓁", path: "image/吳盈蓁.jpg" },
  { type: "image", name: "趙國綱", path: "image/趙國綱.jpg" },
  { type: "image", name: "陳清覦", path: "image/陳清覦.jpg" },
  { type: "image", name: "張安秀", path: "image/張安秀.jpg" },
  { type: "image", name: "洪恩華", path: "image/洪恩華.jpg" },
  { type: "image", name: "胡君如", path: "image/胡君如.jpg" },
  { type: "image", name: "馬郁涵", path: "image/馬郁涵.jpg" },
  { type: "image", name: "劉沅沅", path: "image/劉沅沅.jpg" },
];